//Task a
#include <iostream>;

using namespace std;
int main()
{
    unsigned int n;
    cout << "Enter the integer number: ";
    cin >> n;

    unsigned int i{ 1 };
    double sum{ 0 };
    while (i <= n)
    {
        sum += 1. / i;
        ++i;
    }
    cout << sum << endl;
}