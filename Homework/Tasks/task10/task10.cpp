//Task i
#define _USE_MATH_DEFINES
#include <cmath>
#include <iostream>

using namespace std;
int main()
{
    unsigned int n;
    cout << "Enter the integer number: ";
    cin >> n;

    unsigned int i{ 1 };
    double sum{ 0 };
    double sum_sin{ 0 };
    while (i <= n)
    {
        sum_sin += sin(i * M_PI / 180);
        sum += 1 / sum_sin;
        ++i;
    }
    cout << sum << endl;
}
