//Task b
#include <iostream>

using namespace std;
int main()
{
    unsigned int n;
    cout << "Enter the integer number: ";
    cin >> n;

    unsigned int i{ 1 };
    double product{ 1 };

    while (i <= n)
    {
        product *= 1 + 1. / i / i;
        ++i;
    }
    cout << product << endl;
}