#include <iostream>;

using namespace std;
int main()
{
    unsigned int n;
    cout << "Enter the integer number: ";
    cin >> n;

    unsigned int i{ 1 };
    double sum{ 0 };
    double k{ -1 };
    while (i <= n)
    {
        sum += k / (2 * i + 1);
        k = -k;
        ++i;
    }
    cout << sum << endl;
}
