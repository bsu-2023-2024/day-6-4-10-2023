//Task b from methodical book
#include <iostream>

using namespace std;
int main()
{
    unsigned int n;
    cout << "Enter the integer number: ";
    cin >> n;

    unsigned int i{ 1 };
    double sum{ 1 };
    while (i <= n)
    {
        sum *= 2;
        ++i;
    }
    cout << sum << endl;
}