//Task d
#include <iostream>

using namespace std;
int main()
{
    unsigned int n;
    cout << "Enter the integer number: ";
    cin >> n;

    unsigned int i{ 1 };
    double k{ 1 };
    double sum{ 0 };
    while (i <= n)
    {
        sum += k / i / (i + 1);
        ++i;
        k = -k;
    }
    cout << sum << endl;
}
