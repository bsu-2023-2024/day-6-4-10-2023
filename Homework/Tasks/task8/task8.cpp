//Task e
#include <iostream>

using namespace std;
int main()
{
    unsigned int n;
    cout << "Enter the integer number: ";
    cin >> n;

    unsigned int i{ 1 };
    double sum{ 0 };
    while (i <= n)
    {
        sum += 1. / (2 * i + 1) / (2 * i + 1);
        ++i;
    }
    cout << sum << endl;
}
