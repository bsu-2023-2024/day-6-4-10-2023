//Task g
#include <iostream>

using namespace std;

int main() 
{
	int n;
	cout << "Enter the integer number: ";
	cin >> n;
	unsigned long int i{ 1 };
	long double sum{ 0 };
	unsigned long long int factor{ 1 };
	long double denominator{ 0 };

	while (i <= n)
	{
		factor *= i;
		denominator += 1. / i;
		sum += factor / denominator;
		i++;
	}
	cout << "The result is: " << sum << endl;
}