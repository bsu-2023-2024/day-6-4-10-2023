//This code was modified by Ivan Konovalov, student of the 1 course 2 group.
//He was interested in supporting someone's else code)

#include <iostream>
#include <cmath>

using namespace std;

int	inputData();
double sumOne(int n);
double sumTwo(int n);
double sumThree(int n);
double sumFour(int n);
double sumFive(int n);
void testCasesForSumOne();
void testCasesForSumTwo();
void testCasesForSumThree();
void testCasesForSumFour();
void testCasesForSumFive();
void userModeOne();
void userModeTwo();
void userModeThree();
void userModeFour();
void userModeFive();


int main()
{
	cout << "Enter 1 - to run application in test mode" << endl;
	cout << "Enter 2 - to run application in user mode" << endl;
	cout << "Otherwise  - to exit" << endl;
	int mode;
	cin >> mode;

	cout << "Enter 1, 2, 3, 4 or 5 - number of task" << endl;
	int task;
	cin >> task;
	system("cls");

	switch (mode)
	{
	case 1:
		switch (task)
		{
		case 1:
			testCasesForSumOne();
			break;
		case 2:
			testCasesForSumTwo();
			break;
		case 3:
			testCasesForSumThree();
			break;
		case 4:
			testCasesForSumFour();
			break;
		case 5:
			testCasesForSumFive();
			break;
		default:
			return 0;
		}
		break;
	case 2:
		switch (task)
		{
		case 1:
			userModeOne();
			break;
		case 2:
			userModeTwo();
			break;
		case 3:
			userModeThree();
			break;
		case 4:
			userModeFour();
			break;
		case 5:
			userModeFive();
			break;
		default:
			return 0;
		}
	default:
		return 0;
	}
}

#pragma region userMode
void userModeOne()
{
	int n = inputData();
	cout << "1/1 + 1/2 + ... + 1/" << n << " = " << sumOne(n) << endl;
}

void userModeTwo()
{
	int n = inputData();
	cout << "(1 + 1/1)*(1 + 1/2) * ... * (1 + 1/" << n << ") = " << sumTwo(n) << endl;
}

void userModeThree()
{
	int n = inputData();
	cout << "1/1^5 + 1/2^5 + ... + 1/" << n << "^5 = " << sumThree(n) << endl;
}

void userModeFour()
{
	int n = inputData();
	cout << "-1/3 + 1/5 + ... + (-1)^" << n << "/(2*" << n << "+1) = " << sumFour(n) << endl;
}

void userModeFive()
{
	int n = inputData();
	cout << "sqrt(2 + sqrt(2) + ... + sqrt(2)) " << n << " times = " << sumFive(n) << endl;
}
#pragma endregion

int	inputData()
{
	int n;

	while (true)
	{
		cout << "Enter n > 0 : ";
		cin >> n;
		if (n > 0)
		{
			return n;
		}
		cout << "Invalid n. Try again." << endl;
		system("pause");
		system("cls");
	}
}

#pragma region testCases
void testCasesForSumOne()
{
	cout << (abs(sumOne(6) - 2.4499999999999997) <= 0 ? "Test passed" : "Test failed") << endl;
	cout << (abs(sumOne(10) - 2.9289682539682538) <= 0 ? "Test passed" : "Test failed") << endl;
	cout << (abs(sumOne(100) - 5.187377517639621) <= 0 ? "Test passed" : "Test failed") << endl;
	cout << (abs(sumOne(1000000) - 14.392726722864989) <= 0 ? "Test passed" : "Test failed") << endl;
	cout << (abs(sumOne(10000000) - 16.695311365857272) <= 0 ? "Test passed" : "Test failed") << endl;
	system("pause");
}

void testCasesForSumTwo()
{
	cout << (abs(sumTwo(6) - 3.154706790123457) <= 0 ? "Test passed" : "Test failed") << endl;
	cout << (abs(sumTwo(10) - 3.342847116573682) <= 0 ? "Test passed" : "Test failed") << endl;
	cout << (abs(sumTwo(100) - 3.639682294531309) <= 0 ? "Test passed" : "Test failed") << endl;
	cout << (abs(sumTwo(1000000) - 3.676074234300874) <= 0 ? "Test passed" : "Test failed") << endl;
	cout << (abs(sumTwo(10000000) - 3.6760775427785917) <= 0 ? "Test passed" : "Test failed") << endl;
	system("pause");
}

void testCasesForSumThree()
{
	cout << (abs(sumThree(6) - 1.036790389660494) <= 0 ? "Test passed" : "Test failed") << endl;
	cout << (abs(sumThree(10) - 1.0369073413446939) <= 0 ? "Test passed" : "Test failed") << endl;
	cout << (abs(sumThree(100) - 1.0369277526929555) <= 0 ? "Test passed" : "Test failed") << endl;
	cout << (abs(sumThree(1000000) - 1.036927755143338) <= 0 ? "Test passed" : "Test failed") << endl;
	cout << (abs(sumThree(10000000) - 1.036927755143338) <= 0 ? "Test passed" : "Test failed") << endl;
	system("pause");
}

void testCasesForSumFour()
{
	cout << (abs(sumFour(6) - -0.17906537906537906) <= 0 ? "Test passed" : "Test failed") << endl;
	cout << (abs(sumFour(10) - -0.1919210476486018) <= 0 ? "Test passed" : "Test failed") << endl;
	cout << (abs(sumFour(100) - -0.21212664973225237) <= 0 ? "Test passed" : "Test failed") << endl;
	cout << (abs(sumFour(1000000) - -0.21460158660281903) <= 0 ? "Test passed" : "Test failed") << endl;
	cout << (abs(sumFour(10000000) - -0.2146018116025734) <= 0 ? "Test passed" : "Test failed") << endl;
	system("pause");
}

void testCasesForSumFive()
{
	cout << (abs(sumFive(6) - 1.9993976373924085) <= 0 ? "Test passed" : "Test failed") << endl;
	cout << (abs(sumFive(10) - 1.9999976469034038) <= 0 ? "Test passed" : "Test failed") << endl;
	cout << (abs(sumFive(100) - 2) <= 0 ? "Test passed" : "Test failed") << endl;
	cout << (abs(sumFive(1000000) - 2) <= 0 ? "Test passed" : "Test failed") << endl;
	cout << (abs(sumFive(10000000) - 2) <= 0 ? "Test passed" : "Test failed") << endl;
	system("pause");
}

#pragma endregion
#pragma region tasksSolutions

double sumOne(int n)
{
	double sum = 0;

	for (int i = 1; i <= n; i++)
	{
		sum += 1. / i;
	}

	return sum;
}

double sumTwo(int n)
{
	double product = 1;
	unsigned int i = 1;

	while (i <= n)
	{
		product *= 1 + 1. / i / i;
		++i;
	}

	return product;
}

double sumThree(int n)
{
	unsigned int i{ 1 };
	double sum{ 0 };

	while (i <= n)
	{
		sum += 1. / pow(i, 5);
		++i;
	}

	return sum;
}

double sumFour(int n)
{
	unsigned int i{ 1 };
	double sum{ 0 };

	while (i <= n)
	{
		sum += pow(-1, i) / (2 * i + 1);
		++i;
	}

	return sum;
}

double sumFive(int n)
{
	unsigned int i{ 1 };
	double sum{ 0 };

	while (i <= n)
	{
		sum = sqrt(2 + sum);
		++i;
	}

	return sum;
}

#pragma endregion
